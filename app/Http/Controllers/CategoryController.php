<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function catcat($id)
    {
        $cat = \App\Category::where('id', $id)->get();
        $cname = $cat->name;
        $products = \App\Product::where('category_id', $id)->get();
        $properties = \App\Property::where('category_id', $id)->get();
        $provs = array();
        foreach ($products as $product)
        {
            $prid = $product->id;
            $arr = \App\Product_value::where('product_id', $prid)->get();
            foreach ($arr as $ar)
            {
                array_push($provs, $ar);
            }
        }
    }

    public function CategoryIndex()
    {
        $arCategories = \App\Category::all();
        //dd($arCategories->first()->products->first()->property->where('name', 'photos')->first()->productValues->first()->property_value);

        return view('welcome', ['categories' => $arCategories]);
    }

    public function getCategoryById($id) {
        $arCategories = \App\Category::all();
        $arCategory = \App\Category::find($id);
        $arProducts = $arCategory->products;
        //dd($arCategory->properties->where('name', 'Цвет')->first()->productValues->where('property_value', "Чёрный"));
        return view('catalog', ['categories' => $arCategories, 'category' => $arCategory, 'products' => $arProducts]);
    }

    public function postCategoryById(Request $request, $id) {
        $arCategories = \App\Category::all();
        $arCategory = \App\Category::find($id);
        $arPrV = \App\Product_value::all();

        if($request->startPrice != null and $request->endPrice != null) $arProductsV = $arCategory->properties->where('name', 'Цена')->first()->productValues->where('property_value', '>=', $request->startPrice)->where('property_value', '<=', $request->endPrice);
        else if($request->startPrice != null) $arProductsV = $arCategory->properties->where('name', 'Цена')->first()->productValues->where('property_value', '>=', $request->startPrice);
        else if($request->endPrice != null) $arProductsV = $arCategory->properties->where('name', 'Цена')->first()->productValues->where('property_value', '<=', $request->endPrice);
        else $arProductsV = $arCategory->properties->where('name', 'Цена')->first()->productValues;

        //dd($arProductsV);
        $arProducts = array();

        $arrayPrName = array();
        foreach ($request->arrayFilter as $filter) {
            //dd($arPrV->where('property_value', $filter)->first()->property->name);
            $arrayPrName[$arPrV->where('property_value', $filter)->first()->property->name][] = $filter;
        }
        //dd($arrayPrName);

        foreach ($arProductsV as $arProductV) {
            $i = 1;
            if ($i == 1) {
                foreach ($arrayPrName as $PrName) {

                        $j = 0;
                        foreach ($PrName as $PrNameMin) {
                            //dd($PrNameMin);
                            if ($arPrV->where('property_value', $PrNameMin)->where('product_id', $arProductV->product->id) != "[]") $j = 1;
                        }
                        //dd($j);
                        if ($j == 0) { $i = 0; }
                }

            }




            //dd($arProductV->product);
            if ($i == 1) $arProducts[] = $arProductV->product;
            //dd($arProducts);
        }
        //dd($arProducts);
        return view('catalog', ['categories' => $arCategories, 'category' => $arCategory, 'products' => $arProducts]);
        //return redirect('/catalog/' . $id);
    }


}
