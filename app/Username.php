<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Username extends Model
{
    protected $table = "usernames";
    public function user(){
        return $this->belongsTo('user');
    }
}
