<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    protected $table = "basket";
    public function username(){
        return $this->belongsTo('username');
    }
    public function product(){
        return $this->belongsTo('product');
    }
}
