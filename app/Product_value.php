<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_value extends Model
{
    protected $table = "product_values";
    public function product(){
        return $this->belongsTo('App\Product');
    }
    public function property(){
        return $this->belongsTo('App\Property');
    }
}
