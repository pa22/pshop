<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'CategoryController@categoryIndex');

Route::get('/catalog/{id}', 'CategoryController@getCategoryById');

Route::post('/catalog/{id}', 'CategoryController@postCategoryById');

/*Route::get('/catalog', function () {
    return view('catalog');
});*/

Route::get('/resources/{folder}/{file}', function ($folder, $file) {
    $file_name = File::get(resource_path() . "/" . $folder . "/" . $file);
    $response = Response::make($file_name);
    if (explode(".", $file)[1] == "css") {
        $response->header('Content-Type', "text/css");
    } elseif (explode(".", $file)[1] == "js")
        $response->header('Content-Type', "text/js");
    elseif (explode(".", $file)[1] == "jpg")
        $response->header('Content-Type', "img/jpg");
    elseif (explode(".", $file)[1] == "png")
        $response->header('Content-Type', "img/png");
    return $response;
});
