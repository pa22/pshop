<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arName = [
            'Женская толстовка', 'Мужская толстовка', 'Женская рубашка поло', 'Толстовки (Design #2)', 'Мужская толстовка', 'Мужская футболка', 'Чехол Iphone 7+', 'Чехол Iphone 7+', 'Чехол Iphone 7+', 'Футболка спортивная (dsn 1)', 'Спортивная футболка'
        ];

        $arCategoryId = [
            1, 1, 2, 1, 1, 2, 3, 3, 3, 2, 2
        ];

        for($i = 0; $i < 11; $i++){
            DB::table('products')->insert([
                'name' => $arName[$i],
                'category_id' => $arCategoryId[$i],
            ]);
        }
    }
}
