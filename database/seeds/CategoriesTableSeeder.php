<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arName = [
            'Толстовки', 'Футболки', 'Чехлы на телефон'
        ];

        for($i = 0; $i < 3; $i++){
            DB::table('categories')->insert([
                'name' => $arName[$i],
            ]);
        }
    }
}
