<?php

use Illuminate\Database\Seeder;

class Product_valuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arPropertyId = [
            1, 2, 2, 2, 2, 3, 4, 4, 1, 1, 2, 2, 2, 3, 4, 4, 4, 4, 5, 6, 6, 6, 7, 8, 8, 1, 1, 2, 2, 2, 2, 2, 3, 4, 4, 4, 4, 1, 1, 2, 2, 2, 2, 3, 4, 4, 4, 4, 5, 5, 6, 6, 6, 7, 8, 8, 8, 8, 8, 9, 10, 11, 11, 9, 9, 10, 11, 11, 9, 9, 10, 11, 11, 5, 6, 6, 6, 7, 8, 8, 5, 6, 6, 7, 8, 8
        ];

        $arProductId = [
            1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 8, 8, 8, 8, 9, 9, 9, 9, 9, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 11, 11, 11, 11
        ];

        $arPropertyValue = [
            'Белый', 'XS', 'S', 'M', 'L', '2690', 'Eo7KMaaZ2T0.jpg', '7iCORlDP364.jpg', 'Белый', 'Чёрный', 'M', 'L', 'XL', '2690', '2GH1eDfPPhI.jpg', 'C-fUASLoEL4.jpg', 'ey1d6Fidbd8.jpg', 'C8GQc-bGIFw.jpg', 'Чёрный', 'XS', 'S', 'L', '2690', 'NxMmGcY9sHI.jpg', 'Viu8RW_LCO0.jpg', 'Белый', 'Чёрный', 'XS', 'S', 'M', 'L', 'XL', '2690', 'bE3rDRLsKXs.jpg', 'vTegjAoaDMY.jpg', 'oH2TdJOiu8Y.jpg', 'Z7KKrVMENTg.jpg', 'Белый', 'Чёрный', 'M', 'L', 'XL', 'XXL', '2690', 'vFaAQGFAeko.jpg', 'JHX5vG8xEPU.jpg', 'znkKg4JrCC0.jpg', '9MRbTw3D33Y.jpg', 'Белый', 'Чёрный', 'M', 'L', 'XL', '1490', 'B6jb4unhRiI.jpg', 'y0ePKMHPn0.jpg', '7opmLeRva48.jpg', 'xFF4uB3aUXs.jpg', 'Белый',  'Чёрный', '690', '4jLH8biD5vs.jpg', 'KzKTGSqt3qM.jpg', 'Красный', 'Чёрный', '690', 'XI8C5cSVpKo.jpg', '_w1TgfnCRKo.jpg', 'Белый', 'Чёрный', '690', 'L2MJaAWa3r4.jpg', 'LIBvFAPW28Q.jpg', 'Чёрный', 'M', 'L', 'XXL', '1490', 'Rg8zSISYo4s.jpg', 'x6Uzyo5T4VQ.jpg', 'Белый', 'M', 'L', '1490', 'I3fAPqL5Klg.jpg', '8fJa8WFw3hQ.jpg'
        ];

        for($i = 0; $i < 86; $i++){
            DB::table('product_values')->insert([
                'property_id' => $arPropertyId[$i],
                'product_id' => $arProductId[$i],
                'property_value' => $arPropertyValue[$i],
            ]);
        }
    }
}
