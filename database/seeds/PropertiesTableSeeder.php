<?php

use Illuminate\Database\Seeder;

class PropertiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arName = [
            'Цвет', 'Размер', 'Цена', 'photos', 'Цвет', 'Размер', 'Цена', 'photos', 'Цвет', 'Цена', 'photos'
        ];

        $arCategoryId = [
            1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3
        ];

        for($i = 0; $i < 11; $i++){
            DB::table('properties')->insert([
                'name' => $arName[$i],
                'category_id' => $arCategoryId[$i],
            ]);
        }
    }
}
