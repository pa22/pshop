function radioChange(obj) {
    let name = obj.getAttribute("name");
    let value = obj.getAttribute("value");
    let img = document.getElementById(name);
    let imgSrc = img.getAttribute("src").split("/");
    imgSrc[5] = value;
    let imgNewSrc = imgSrc[0] + '/' + imgSrc[1] + '/' + imgSrc[2] + '/' + imgSrc[3] + '/' + imgSrc[4] + '/' + imgSrc[5];
    img.setAttribute("src", imgNewSrc);

}
