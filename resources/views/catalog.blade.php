@extends('app')

@section('content')

    <div class="container catalog_container">
        <!-- FILTERS -->

        <div class="goods_name">{{$category->name}}</div>

        <div class="row">
            <div class="col-3">
                <div class="filter_inner">
                <form action="/catalog/{{$category->id}}" method="POST">
                    {{ csrf_field() }}
                <div class="filter_section">
                    <h6>Категория</h6>
                    <ul class="filter_section-inner">
                        @foreach($categories as $categoryOne)
                        <li><label><a href="/catalog/{{$categoryOne->id}}"><span>{{$categoryOne->name}}</span></a></label></li>
                        @endforeach
                    </ul>
                    <hr class="filter_hr">
                </div>

                @foreach($category->properties as $property)
                @php ($array = array())
                @if($property->name != "photos") @if($property->name != "Цена")
                <div class="filter_section">
                    <h6>{{$property->name}}</h6>
                    <ul class="filter_section-inner @if($property->name == "Размер") size @endif">
                        @foreach($property->productValues as $productValue)
                            @if(!in_array($productValue->property_value, $array))
                            @php ($array[] = $productValue->property_value)
                        <li><label><input type="checkbox" name="arrayFilter[]" value="{{$productValue->property_value}}"><span>@if($property->name != 'Цвет') {{$productValue->property_value}} @else {{$productValue->property_value  ." (".$category->properties->where('name', $property->name)->first()->productValues->where('property_value', $productValue->property_value)->count().")" }} @endif</span></label></li>
                            @endif
                        @endforeach
                    </ul>
                    <hr class="filter_hr">
                </div>
                @endif @endif
                @endforeach

                <div class="filter_section price">
                    <h6>Цена</h6>

                    <div class="custom_price">
                        <div class="col-6 label">
                            <label for="start_price">от</label>
                        </div>

                        <div class="col-6 label">
                            <label for="end_price">до</label>
                        </div>

                        <div class="col-6 price">
                            <input id="start_price" name="startPrice">
                        </div>

                        <div class="col-6 price">
                            <input id="end_price" name="endPrice">
                        </div>

                    </div>
                </div>

                    <div class="filter_submit_inner">
                        <input type="submit" value="Найти" class="filter_submit">
                    </div>

                </form>
                </div>
            </div> <!-- FILTERS--end -->

            <!-- GOODS -->

            <div class="col-9 goods_inner">
                <div class="row">
                    @foreach($products as $product)
                    <div class="col-4 good_inner">
                        <a class="good_image" href="#">
                            <img src="{{ asset('resources/img/' . $product->property->where('name', 'photos')->first()->productValues->where('product_id', $product->id)->first()->property_value) }}" id="good_{{$product->id}}">
                            <div class="good_gradient">
                                <div class="good_gradient_content">
                                    @php ($i = 0)
                                    @foreach($product->property->where('name', 'photos')->first()->productValues->where('product_id', $product->id) as $photo)
                                    <div class="good_icon">
                                        <label><input type="radio" name="good_{{$product->id}}" @if($i === 0) checked @endif value="{{ $photo->property_value }}" onchange="radioChange(this)"><img src="{{ asset('resources/img/' . $photo->property_value) }}"></label>
                                    </div>
                                    @php ($i++)
                                    @endforeach
                                </div>
                            </div>
                        </a>


                        <p>{{$product->name}}</p>
                        <span class="good_price">{{$product->property->where('name', 'Цена')->first()->productValues->where('product_id', $product->id)->first()->property_value}} &#8381;</span>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection