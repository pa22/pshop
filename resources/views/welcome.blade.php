@extends('app')

@section('content')


<!-- <div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-3">Добро пожаловать!</h1>
    <p class="lead">Начинается разработка данного проекта.</p>
  </div>
</div> -->



<!-- INTRO -->
    <div class="intro">

        <div class="container">

            <h1>Интернет-магазин <br>товаров МосПолитеха</h1>
            <p>Добро пожаловать!<br>
                Это интернет-магазин с мерчем Московского Политеха.
                <br>Наши дизайнеры создали линию одежды с символикой нашего любимого университета.<br>Теперь приобрести разные вещи с символом Московского Политеха станет куда проще.
            </p>

        </div>

    </div>
<!-- INTRO--END -->


<!-- CATALOG -->
    <div class="catalog">
        <div class="container">
            <div class="catalog_top">
                <h3>Что вас интересует?</h3>

                <a class="show_more" href="#">показать больше</a>
            </div>


            <div class="row catalog_images">
                @foreach($categories->take(3) as $category)
                <div class="col-4">
                    <a class="catalog_image" href="catalog/{{$category->id}}">
                        <img src="{{ asset('resources/img/'.$category->products()->first()->property()->where('name', 'photos')->first()->productValues()->first()->property_value) }}">
                        <div class="catalog_gradient">
                            <div class="catalog_gradient_content">
                                <div><i class="fas fa-hand-point-up"></i></div>
                                <div>перейти в раздел</div>
                            </div>
                        </div>
                    </a>
                </div>
                @endforeach
            </div>

            <div class="row catalog_images_names">
                @foreach($categories as $category)
                <div class="col-4">
                    <span>{{$category->name}}</span>
                </div>
                    @endforeach
            </div>
        </div>
    </div>
<!-- CATALOG--END -->

@endsection