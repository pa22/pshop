<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('resources/css/style.css') }}">
	<title>PolyMerch</title>
</head>
<body>

@if(Request::url() == "http://pa22.std-842.ist.mospolytech.ru")
<div class="screen">
@endif

<nav class="navbar navbar-expand-md navbar-light shadow-lg">
	<div class="container">

		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<ul class="navbar-nav mr-auto">
				<a class="nav-item nav-link" href="#">Каталог</a>
				<a class="nav-item nav-link" href="#">Контакты</a>
				<a class="nav-item nav-link" href="#">О магазине</a>
				<a class="nav-item nav-link" href="#">Доставка</a>
				<a class="nav-item nav-link" href="#">Корзина</a>
			</div>
		</div>

	</div>
</nav>

<!--
	<nav class="navbar navbar-expand-lg navbar-light navbar-custom">
		<div class="container-fluid">
			<a href="/" class="navbar-brand mainlink">
            	<img src="https://mospolytech.ru/storage/b53b3a3d6ab90ce0268229151c9bde11/images/Logo2.jpg" width="150" alt="" class="d-inline-block align-middle mr-2">
            	<span>Интернет-магазин товаров Московского Политеха</span>
        	</a>
			<div class="navbar-header">
				<a class="mainlink nav-item active" href="/">Главная страница</a>
			</div> -->
			<!-- <ul class="nav navbar-nav">
				<li class="active"><a href="/">Home</a></li>
				<li><a href="#">Page 1</a></li>
				<li><a href="#">Page 2</a></li>
				<li><a href="#">Page 3</a></li>
			</ul> -->
<!-- </div>
	</nav>
	<div class="wrapper">
		<div class="content"> -->