<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('resources/css/style.css') }}">

    <title>PolyMerch</title>
</head>
<body>

        <nav class="navbar navbar-expand-md shadow @if(Request::url() == "http://pa22.std-842.ist.mospolytech.ru") fixed-top @endif">
            <div class="container">

                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <ul class="navbar-nav mr-auto">
                        <a class="nav-item nav-link" href="/catalog/1">Каталог</a>
                        <a class="nav-item nav-link" href="#">Контакты</a>
                        <a class="nav-item nav-link" href="#">О магазине</a>
                        <a class="nav-item nav-link" href="#">Доставка</a>
                        <a class="nav-item nav-link" href="#">Корзина</a>
                    </ul>
                </div>
            </div>


    </nav>

    @yield('content')
        <!-- CONTACTS -->
        <div class="contacts">
            <div class="container">
                <h3>Как с нами связаться?</h3>

                <div class="row">
                    <div class="col-6">
                        <h6>Наш Адрес</h6>
                        <p>107023, г. Москва, ул. Большая Семеновская, 38.</p>
                    </div>
                    <div class="col-6">
                        <h6>Наш телефон</h6>
                        <p>+7 (495) 223-05-23</p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <h6>Наша почта</h6>
                        <p>merchpolytech@mospolytech.ru</p>
                    </div>
                    <div class="col-6">
                        <h6>Мы в соцсетях</h6>
                        <div class="social_network">

                            <a href="#"><i class="fab fa-facebook-square"></i></a>
                            <a href="#"><i class="fab fa-instagram-square"></i></a>
                            <a href="#"><i class="fab fa-youtube-square"></i></a>
                            <a href="#"><i class="fab fa-vk"></i></a>

                        </div>
                    </div>
                </div>

                <hr>

                <div class="copyright">
                    <span>&#169; {{ date('Y') }} Московский политехнический университет</span>
                </div>


            </div>
        </div>
    <script src="https://kit.fontawesome.com/fce4fb2e93.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="{{ asset('resources/js/script.js') }}"></script>
</body>

</html>
